export const origins = [process.env.ADMIN_APP_BASE_URL, process.env.APP_BASE_URL];

export const SUCCESSFUL_REGISTRATION = 'Successful registration';
export const EMAIL_IS_ALREADY_EXISTS = 'The provided email address already exists';

export const EMAIL_ACTIVATION_TOKEN_EXPIRAION_MS = 3600000 * 24 * 3; // 3 days
export const ACCESS_TOKEN_EXPIRY_SEC = 3600 * 2; // 2 hours
export const REFRESH_TOKEN_EXPIRY_SEC = 3600 * 6; // 6 hours
