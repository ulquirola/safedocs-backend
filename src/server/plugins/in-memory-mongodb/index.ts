import fp from 'fastify-plugin';
import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';

export default fp(async (fastify, opts, next) => {
  const mongoServer = await MongoMemoryServer.create();

  mongoose.Promise = Promise;

  const mongoUri = mongoServer.getUri();

  const connect = async (mongoUri) => {
    mongoose.connection.on('connected', () => {
      fastify.log.info({ actor: 'MongoDB' }, 'connected');
    });

    mongoose.connection.on('disconnected', () => {
      mongoServer.stop();

      fastify.log.error({ actor: 'MongoDB' }, 'disconnected');
    });

    const database = await mongoose.connect(mongoUri, {
      dbName: process.env.DB_NAME,
      keepAlive: true,
    });

    fastify.decorate('dbConnection', database.connection);

    next();
  };
  await connect(mongoUri);
});
