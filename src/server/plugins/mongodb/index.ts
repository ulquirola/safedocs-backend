import fp from 'fastify-plugin';
import { connect, connection } from 'mongoose';

export default fp(async (fastify, opts, next) => {
  const uri = `mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@mongo:${process.env.DB_PORT}/`;

  connection.on('connected', () => {
    fastify.log.info({ actor: 'MongoDB' }, 'connected');
  });

  connection.on('disconnected', () => {
    fastify.log.error({ actor: 'MongoDB' }, 'disconnected');
  });

  const database = await connect(uri, {
    keepAlive: true,
    dbName: process.env.DB_NAME,
  });

  fastify.decorate('dbConnection', database.connection);

  next();
});
