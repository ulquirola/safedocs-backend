import { build } from './app';

// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();

const server = build({
  pluginTimeout: 20000,
});
const start = async () => {
  try {
    await server.listen(process.env.APP_PORT, process.env.APP_HOST);
    server.blipp();
  } catch (err) {
    console.log(err, 'Server error');
    process.exit(1);
  }
};
start();
