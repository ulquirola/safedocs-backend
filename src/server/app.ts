import fastify from 'fastify';
import fastifyBlipp from 'fastify-blipp';
import fastifyMulter from 'fastify-multer';
import fasitfyStatic from 'fastify-static';
import fastifySwagger from 'fastify-swagger';
import fastifyCors from 'fastify-cors';

import inMemoryMongoDB from './plugins/in-memory-mongodb';
import mongoDB from './plugins/mongodb';

import i18next from 'i18next';
import i18nextMiddleware from 'i18next-http-middleware';

import path from 'path';

import swagger from '../config/swagger';
import { origins } from '../resources/constants';
import { localizationConfig } from '../i18';

import AuthRoutes from '../modules/routes/auth';

import routeAccessMiddleware from '../modules/hooks/routeAccess';

import { FastifyServer, CustomFastifyRequest, Reply } from 'modules/controllers/types';
import { Environment } from '../types';

import { ErrorService } from '../services/error-service';

export const build = (opts = {}): FastifyServer => {
  const server: FastifyServer = fastify({
    logger: true,
    pluginTimeout: 120000,
    ajv: {
      customOptions: { allErrors: true, jsonPointers: true },
    },
    ...opts,
  });

  registerPlugins(server);
  registerDatabase(server);
  registerRoutes(server);
  registerHooks(server);

  addErrorHandlers(server);

  return server;
};

const registerPlugins = (server: FastifyServer) => {
  server.register(fastifyBlipp);
  server.register(fastifyMulter.contentParser);
  server.register(fasitfyStatic, { root: path.join(__dirname, '/') });
  server.register(fastifySwagger, swagger.options);
  server.register(fastifyCors, { origin: origins });
};

const registerDatabase = (server: FastifyServer) => {
  if (process.env.NODE_ENV === Environment.Test) {
    server.register(inMemoryMongoDB);
  } else {
    server.register(mongoDB);
  }
};

const registerRoutes = (server: FastifyServer) => {
  server.register(AuthRoutes);
};

const registerHooks = (server: FastifyServer) => {
  // add plugin: i18nextMiddleware
  i18next.use(i18nextMiddleware.LanguageDetector).init(localizationConfig);

  // i18nextMiddleware plugin registers itself with preHandler hook, but we will need the t() function in onRequest hook for routeAccessMiddleware
  const middleware = i18nextMiddleware.handle(i18next);

  server.addHook('onRequest', (request: CustomFastifyRequest, reply: Reply, next) =>
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    middleware(request, reply, next),
  );

  // add plugin: routeAccessMiddleware
  server.addHook('onRequest', (request: CustomFastifyRequest, reply: Reply, next) => {
    routeAccessMiddleware(request, reply, next);
  });
};

const addErrorHandlers = (server: FastifyServer) => {
  server.setErrorHandler(function (error, request: CustomFastifyRequest, reply) {
    ErrorService.handleError(error, request, reply);
  });
};
