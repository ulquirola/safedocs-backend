import { Environment } from '../types';

export default {
  options: {
    routePrefix: '/documentation',
    exposeRoute: process.env.NODE_ENV === Environment.Development,
    swagger: {
      info: {
        title: 'Safedocs API',
        description: 'API for safedocs service',
        version: '1.0.0',
      },
      externalDocs: {
        url: 'https://swagger.io',
        description: 'Find more info here',
      },
      host: 'localhost',
      schemes: ['http'],
      consumes: ['application/json'],
      produces: ['application/json'],
    },
  },
};
