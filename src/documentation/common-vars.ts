export const commonProperties = {
  consumes: ['application/json'],
  produces: ['application/json'],
};

export const generalError = {
  description: 'General error',
  type: 'object',
  properties: {
    message: { type: 'string' },
  },
};
