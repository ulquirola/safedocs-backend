import { EMAIL_IS_ALREADY_EXISTS, SUCCESSFUL_REGISTRATION } from '../resources/constants';
import { generalError, commonProperties } from './common-vars';
import { PASSWORD_REGEX } from '../utils/common';

const userProps = {
  _id: { type: 'string' },
  email: { type: 'string', format: 'email' },
  firstName: { type: 'string' },
  lastName: { type: 'string' },
  company_id: { type: 'string' },
  roles: { type: 'array', items: { type: 'string' } },
  isActivated: { type: 'boolean' },
};

export const registerChecker = {
  schema: {
    description: 'Register checker user',
    tags: ['auth'],
    summary: 'Registers a new user with checker role',
    consumes: ['application/json'],
    body: {
      type: 'object',
      required: ['email', 'firstName', 'lastName', 'company_id'],
      properties: {
        email: { type: 'string', format: 'email' },
        firstName: { type: 'string' },
        lastName: { type: 'string' },
        company_id: { type: 'string' },
      },
      additionalProperties: false,
    },
    response: {
      201: {
        description: SUCCESSFUL_REGISTRATION,
        type: 'object',
        properties: {
          message: { type: 'string' },
          data: {
            type: 'object',
            properties: userProps,
          },
        },
      },
      400: generalError,
      422: {
        description: EMAIL_IS_ALREADY_EXISTS,
        type: 'object',
        properties: {
          message: { type: 'string' },
        },
      },
    },
  },
};

export const registerAdmin = {
  schema: {
    description: 'Register admin user',
    tags: ['auth'],
    summary: 'Registers a new admin user',
    consumes: ['application/json'],
    body: {
      type: 'object',
      required: ['email', 'firstName', 'lastName', 'company_id'],
      properties: {
        email: { type: 'string', format: 'email' },
        firstName: { type: 'string' },
        lastName: { type: 'string' },
        company_id: { type: 'string' },
      },
      additionalProperties: false,
    },
    response: {
      201: {
        description: SUCCESSFUL_REGISTRATION,
        type: 'object',
        properties: {
          message: { type: 'string' },
          data: {
            type: 'object',
            properties: userProps,
          },
        },
      },
      400: generalError,
      422: {
        description: EMAIL_IS_ALREADY_EXISTS,
        type: 'object',
        properties: {
          message: { type: 'string' },
        },
      },
    },
  },
};

export const login = {
  schema: {
    ...commonProperties,
    description: 'Login a user',
    tags: ['auth'],
    summary: 'Logs in the specified user',
    body: {
      type: 'object',
      required: ['email', 'password'],
      properties: {
        email: { type: 'string' },
        password: {
          type: 'string',
          pattern: PASSWORD_REGEX,
        },
      },
      additionalProperties: false,
    },
    response: {
      200: {
        description: 'Successful login',
        type: 'object',
        properties: {
          data: {
            type: 'object',
            properties: {
              accessToken: { type: 'string' },
              refreshToken: { type: 'string' },
              user: {
                type: 'object',
                properties: userProps,
              },
            },
          },
        },
      },
      400: generalError,
      401: {
        description: 'Password is not correct',
        type: 'object',
        properties: {
          message: { type: 'string' },
        },
      },
      404: {
        description: 'User does not exist',
        type: 'object',
        properties: {
          message: { type: 'string' },
        },
      },
    },
  },
};
