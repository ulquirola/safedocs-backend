export enum Environment {
  Production = 'production',
  Development = 'development',
  Test = 'test',
}
export interface TokenData {
  token: string;
  tokenHash: string;
}
