import { UserRole } from '../../modules/models/user.model';

export const PUBLIC_ROUTES = {
  '/auth/register-admin': ['POST'], // Leave it here for testing now
  '/auth/register-checker': ['POST'], // Leave it here for testing now
  '/auth/login': ['POST'],

  '/documentation/*': ['GET'],
};

export const ROUTE_ACCESS = {
  '/auth/register-admin': {
    POST: [UserRole.SuperAdmin],
  },
  '/auth/register-checker': {
    POST: [UserRole.Admin],
  },
};
