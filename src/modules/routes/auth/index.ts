import fp from 'fastify-plugin';
import { registerAdmin, registerChecker, login } from '../../../documentation/auth';
import { authController } from '../../../modules/controllers/auth.controller';
import { CustomFastifyRequest, Reply, FastifyServer } from 'modules/controllers/types';

export default fp(async (server: FastifyServer, opts, next) => {
  server.post(
    '/auth/register-checker',
    registerChecker,
    async (request: CustomFastifyRequest, reply: Reply) => {
      await authController.registerChecker(server, request, reply);
    },
  );

  server.post(
    '/auth/register-admin',
    registerAdmin,
    async (request: CustomFastifyRequest, reply: Reply) => {
      await authController.registerAdmin(server, request, reply);
    },
  );

  server.post('/auth/login-checker', login, async (request: CustomFastifyRequest, reply: Reply) => {
    await authController.checkerLogin(server, request, reply);
  });

  server.post('/auth/login-admin', login, async (request: CustomFastifyRequest, reply: Reply) => {
    await authController.adminLogin(server, request, reply);
  });

  next();
});
