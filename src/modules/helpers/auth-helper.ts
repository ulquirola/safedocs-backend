import { User, UserRole } from '../../modules/models/user.model';
import { generateToken } from '../../utils/common';
import { EMAIL_ACTIVATION_TOKEN_EXPIRAION_MS } from '../../resources/constants';
import { EmailService } from '../../services/email-service';

export const sendActivationEmail = async (user: User) => {
  const { token, tokenHash } = await generateToken();

  await User.findOneAndUpdate(
    { _id: user._id },
    {
      $set: {
        activationToken: tokenHash,
        activationTokenExpiration: Date.now() + EMAIL_ACTIVATION_TOKEN_EXPIRAION_MS,
      },
    },
    { runValidators: true },
  );

  const baseUrl = user.roles.includes(UserRole.Admin)
    ? process.env.ADMIN_APP_BASE_URL
    : process.env.APP_BASE_URL;

  // Later probably we will send different type of email to admins and to the checkers
  EmailService.sendActivationEmail(user.email, {
    firstName: user.firstName,
    activationUrl: `${baseUrl}/auth/activate?token=${token}&id=${user._id}`,
  });
};
