import jwt = require('jsonwebtoken');
import { User } from '../models/user.model';
import { Reply, CustomFastifyRequest } from '../controllers/types';
import UrlPattern = require('url-pattern');
import { PUBLIC_ROUTES, ROUTE_ACCESS } from '../../modules/routes/route-access';

export default async (
  request: CustomFastifyRequest,
  reply: Reply,
  next: () => void,
): Promise<Reply | void> => {
  if (!!request.headers) {
    const rawUrl = request.raw.url;
    const hasParameters = rawUrl.includes('?');
    const path = hasParameters ? rawUrl.split('?')[0] : rawUrl;

    // Check if request url is a public route
    if (
      Object.keys(PUBLIC_ROUTES).some((routePattern) => {
        const pattern = new UrlPattern(routePattern);

        return (
          pattern.match(path.replace('.', '')) !== null &&
          PUBLIC_ROUTES[routePattern].includes(request.raw.method)
        );
      })
    ) {
      return next();
    }

    // Return 401 if request doesn't have token
    if (!request.headers.authorization || !request.headers.authorization.split('Bearer ')[1]) {
      return reply.code(401).send({ message: request.t('auth.unauthorized_no_token') });
    }

    const token = request.headers.authorization.split('Bearer ')[1];

    interface decoded {
      id: string;
      aud: string;
    }

    jwt.verify(
      token,
      process.env.AUTH_SECRET,
      async (err: jwt.JsonWebTokenError, decoded: decoded) => {
        if (err) {
          return reply.code(401).send({ message: err.message });
        }

        const user = await User.findById(decoded.id);

        if (!user) {
          return reply.code(401).send({ message: request.t('auth.unauthorized') });
        }

        if (!!user) {
          request.userId = user._id;
          request.userRoles = user.roles;

          const [route] = rawUrl.split('?');

          // Check if route exists and check if user has permission to it
          const accesToRoute = () => {
            let isRouteFound = false;
            let hasAccess = false;

            for (const routePattern of Object.keys(ROUTE_ACCESS)) {
              const pattern = new UrlPattern(routePattern);

              if (pattern.match(route.replace('.', '')) !== null) {
                isRouteFound = true;

                if (
                  ROUTE_ACCESS[routePattern][request.raw.method] &&
                  ROUTE_ACCESS[routePattern][request.raw.method].includes(decoded.aud) &&
                  ROUTE_ACCESS[routePattern][request.raw.method].some((value) =>
                    user.roles.includes(value),
                  )
                ) {
                  hasAccess = true;
                }

                break;
              }
            }
            return { isRouteFound: isRouteFound, hasAccess: hasAccess };
          };

          if (accesToRoute().isRouteFound) {
            return reply
              .code(404)
              .send({ message: request.t('common.endpoint_not_found') + ': ' + route });
          }

          if (accesToRoute().hasAccess) {
            return reply.code(403).send({ message: request.t('auth.forbidden') });
          }

          return next();
        }
        // error occurred
      },
    );
  } else {
    return next();
  }
};
