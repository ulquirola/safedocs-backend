import mongoose, { Document, Schema, Model, model } from 'mongoose';
import { getHash } from '../../utils/common';

export enum UserRole {
  Checker = 'CHECKER',
  Admin = 'ADMIN',
  SuperAdmin = 'SUPERADMIN',
}

export interface User extends Document {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  company_id: string;
  roles: UserRole[];
  isActivated: boolean;
  activationToken?: string;
  activationTokenExpiration?: number;
  createdAt: Date;
}

export type UserModel = User;

export const UserSchema: Schema = new Schema(
  {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, default: '' },
    company_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Company',
      required: true,
    },
    roles: {
      type: [String],
      enum: Object.values(UserRole),
      required: true,
      default: UserRole.Checker,
    },
    isActivated: { type: Boolean, required: true, default: false },
    activationToken: { type: String, required: false },
    activationTokenExpiration: { type: Number, required: false },
  },
  {
    timestamps: { createdAt: true, updatedAt: false },
    collection: 'users',
    toJSON: {
      virtuals: false,
      versionKey: false,
      transform: (doc, obj) => {
        if (obj.password) {
          // remove sensitive data
          delete obj.password;
        }
        return obj;
      },
    },
    toObject: {
      virtuals: false,
      versionKey: false,
      transform: (doc, obj) => {
        if (obj.password) {
          // remove sensitive data
          delete obj.password;
        }
        return obj;
      },
    },
  },
);

UserSchema.pre<User>('save', async function (next) {
  if (!this.isModified('password')) {
    return next();
  }
  this.password = await getHash(this.password);
  next();
});

export const User: Model<UserModel> = model<UserModel>('User', UserSchema);
