import mongoose, { Document, Schema, Model, model } from 'mongoose';

export interface Company extends Document {
  name: string;
  admins: Array<mongoose.Schema.Types.ObjectId>;
}

export type CompanyModel = Company;

export const CompanySchema: Schema = new Schema(
  {
    name: { type: String, required: true, unique: true },
    admins: [
      {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
      },
    ],
  },
  {
    timestamps: { createdAt: true, updatedAt: false },
    collection: 'companies',
  },
);

export const Company: Model<CompanyModel> = model<CompanyModel>('Company', CompanySchema);
