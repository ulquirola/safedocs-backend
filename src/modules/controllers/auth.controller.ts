import { FastifyServer, CustomFastifyRequest, Reply } from './types';
import { sendActivationEmail } from '../helpers/auth-helper';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { ACCESS_TOKEN_EXPIRY_SEC, REFRESH_TOKEN_EXPIRY_SEC } from '../../resources/constants';
import { User, UserRole } from '../models/user.model';

import {
  BadRequestError,
  AuthenticationError,
  UnauthorizedError,
} from '../../services/error-service/types';
class AuthController {
  registerAdmin = async (
    server: FastifyServer,
    request: CustomFastifyRequest,
    reply: Reply,
  ): Promise<void> => {
    await this.registration(server, request, reply, UserRole.Admin);
  };

  registerChecker = async (
    server: FastifyServer,
    request: CustomFastifyRequest,
    reply: Reply,
  ): Promise<void> => {
    await this.registration(server, request, reply, UserRole.Checker);
  };

  registration = async (
    server: FastifyServer,
    request: CustomFastifyRequest,
    reply: Reply,
    userRole: UserRole,
  ): Promise<void> => {
    request.body.email = request.body.email.toLowerCase();

    const { email } = request.body;

    const user = await User.findOne({ email });

    if (user) {
      throw new BadRequestError('auth.email_already_exists', 422);
    }

    const userToCreate = { ...request.body, roles: [userRole] };

    const newUser = await User.create(userToCreate);

    await sendActivationEmail(newUser);

    return reply
      .code(201)
      .send({ message: request.t('auth.successful_registration'), data: newUser });
  };

  checkerLogin = async (
    server: FastifyServer,
    request: CustomFastifyRequest,
    reply: Reply,
  ): Promise<void> => {
    await this.login(server, request, reply, UserRole.Checker);
  };

  adminLogin = async (
    server: FastifyServer,
    request: CustomFastifyRequest,
    reply: Reply,
  ): Promise<void> => {
    await this.login(server, request, reply, UserRole.Admin);
  };

  login = async (
    server: FastifyServer,
    request: CustomFastifyRequest,
    reply: Reply,
    userRole: UserRole,
  ): Promise<void> => {
    request.body.email = request.body.email.toLowerCase();

    const { email, password } = request.body;

    const user = await User.findOne({ email });

    if (!user) {
      throw new AuthenticationError('auth.user_not_found');
    }

    if (!user.roles.includes(userRole)) {
      throw new UnauthorizedError();
    }

    const isPasswordRight = bcrypt.compareSync(password, user.password);

    if (!isPasswordRight) {
      throw new AuthenticationError('auth.password_incorrect');
    }

    if (!user.isActivated) {
      throw new UnauthorizedError('auth.user_not_activated');
    }

    const accessToken = jwt.sign({ id: user.id }, process.env.AUTH_SECRET, {
      audience: userRole,
      expiresIn: ACCESS_TOKEN_EXPIRY_SEC,
    });

    const refreshToken = jwt.sign({ id: user.id }, process.env.AUTH_SECRET, {
      audience: userRole,
      expiresIn: REFRESH_TOKEN_EXPIRY_SEC,
    });

    if (accessToken) {
      return reply.code(200).send({
        data: {
          accessToken,
          refreshToken,
          user,
        },
      });
    }
  };
}

const authController = new AuthController();

export { authController };
