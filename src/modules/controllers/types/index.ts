import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';
import { Connection } from 'mongoose';

export interface FastifyServer extends FastifyInstance {
  dbConnection?: Connection;
}

export interface CustomFastifyRequest extends FastifyRequest {
  userRoles: string[];
  userId: string;
  body: any;
  t: (s: string) => string;
}

export type Reply = FastifyReply;
