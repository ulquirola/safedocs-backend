import en from './translations/en.json';
import commonConfig from '../config/common';

export const localizationConfig = {
  resources: { en: { translation: en } },
  fallbackLng: commonConfig.defaultLanguage,
};
