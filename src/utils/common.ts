import bcrypt = require('bcryptjs');
import crypto from 'crypto';
import { TokenData } from '../types';

export const getHash = async (value: string, salt = 10): Promise<string> => {
  return await bcrypt.hash(value, salt);
};

export const generateToken = async (size = 32, salt = 10): Promise<TokenData> => {
  const token = crypto.randomBytes(size).toString('hex');
  const tokenHash = await getHash(token, salt);

  return { token, tokenHash };
};

export const PASSWORD_REGEX = '^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9]).{6,}$';
