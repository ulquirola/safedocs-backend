import nodemailer from 'nodemailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
import Email from 'email-templates';
import { EmailTemplate, EmailParams, ActivationEmailTemplateData } from './types';
import { Environment } from '../../types';

export class EmailService {
  static async sendActivationEmail(to: string, data: ActivationEmailTemplateData): Promise<void> {
    await EmailService.sendEmail({
      template: EmailTemplate.Activation,
      from: process.env.EMAIL_FROM,
      to: to,
      locals: data,
    });
  }

  private static getEmailConfig = (): SMTPTransport.Options => {
    if (process.env.NODE_ENV === Environment.Production) {
      return {
        host: process.env.EMAIL_HOST,
        port: parseInt(process.env.EMAIL_PORT),
        secure: true,
        auth: {
          type: 'OAuth2',
          user: process.env.EMAIL_FROM,
          serviceClient: process.env.EMAIL_CLIENT_ID,
          privateKey: process.env.EMAIL_PRIVATE_KEY,
        },
      };
    } else {
      const emailFrom = process.env.EMAIL_FROM;
      const emailPass = process.env.EMAIL_PASS;
      const auth = emailFrom && emailPass ? { user: emailFrom, pass: emailPass } : undefined;

      return {
        service: process.env.EMAIL_SERVICE,
        auth,
      };
    }
  };

  private static async sendEmail(params: EmailParams): Promise<void> {
    const transporter = nodemailer.createTransport(EmailService.getEmailConfig());

    const email = new Email({
      transport: transporter,
      send: true,
      preview: false,
      views: {
        root: `${process.cwd()}/src/views/email-templates`,
      },
    });

    await email.send({
      template: params.template ?? params.template,
      message: {
        from: params.from,
        to: params.to,
        subject: params.subject ?? params.subject,
        attachments: params.attachments ?? params.attachments,
      },
      locals: params.locals ?? params.locals,
    });
  }
}
