export interface BaseEmailTemplateData {
  firstName: string;
}

export interface ActivationEmailTemplateData extends BaseEmailTemplateData {
  activationUrl: string;
}

export interface EmailParams {
  template?: string;
  from: string;
  to: string;
  subject?: string;
  attachments?: {
    filename: string;
    contentType: string;
    path: string;
  }[];
  locals?: ActivationEmailTemplateData | BaseEmailTemplateData;
}

export enum EmailTemplate {
  Activation = 'activation',
}
