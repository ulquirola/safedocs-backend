import { FastifyError } from 'fastify';
import { AuthenticationError, BadRequestError, UnauthorizedError } from './types';
import { FastifyReply } from 'fastify/types/reply';
import { CustomFastifyRequest } from '../../modules/controllers/types';
import { Environment } from '../../types';

export class ErrorService {
  static handleError = (
    error: FastifyError,
    request: CustomFastifyRequest,
    reply: FastifyReply,
  ) => {
    if (error.validation) {
      return reply.code(error.statusCode ?? 400).send({ message: error.message });
    } else if (error instanceof BadRequestError) {
      return reply.code(error.statusCode).send({ message: request.t(error.message) });
    } else if (error instanceof AuthenticationError) {
      const message = request.t(error.message.length ? error.message : 'auth.unauthorized');

      return reply.code(401).send({ message });
    } else if (error instanceof UnauthorizedError) {
      const message = request.t(error.message.length ? error.message : 'auth.forbidden');

      return reply.code(403).send({ message });
    }

    const errorMessage =
      process.env.NODE_ENV === Environment.Production
        ? request.t('common.general_error')
        : error.message;

    return reply.code(500).send({ message: errorMessage });
  };
}
