## Safedocs backend project

### Used technologies

- Typescript
- Fastify
- Husky
- MongoDB and Mongoose

### Running the project:

#### Local development
   - docker-compose up safedocs_be_dev
